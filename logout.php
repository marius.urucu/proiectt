<?php

// Inialize session
session_start();

// Delete certain session
unset($_SESSION['username']);
// Delete all session variables
setcookie("unm", $_SESSION['username'],0);
session_destroy();

// Jump to login page
header('Location: index.html');


