<?php


class DbConnect{

    const DB_HOST = 'localhost';  //  hostname
    const DB_NAME = 'User';  //  databasename
    const DB_USER = 'root';  //  username
    const USER_PW = 'rooturucu';  //  password

    private $dbo;

    public function __construct()
    {
        try {
            $this->dbo = new PDO('mysql:host=' . self::DB_HOST .';dbname=' . self::DB_NAME, self::DB_USER, self::USER_PW);
            $this->dbo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $err) {
            echo "harmless error message if the connection fails";
        }
    }


    public function getDbo()
    {
        return $this->dbo;
    }
}


